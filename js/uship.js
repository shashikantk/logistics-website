/**
 * uShip namespace
 * Defines a library of common utilities

 */

'use strict';

;(function (root, $) {
    'use strict';

    var previousuShip = root.uship;

    var uship;
    if (typeof exports !== 'undefined') {
        uship = exports;
    } else {
        uship = root.uship = {};
    }

    /**
     * Returns a nested namespace. Takes three argument forms:
     *    var module = uship.namespace('path.to.module');
     *    var module = uship.namespace(['path', 'to', 'module']);
     *    var module = uship.namespace('path', 'to', 'module');
     * ...where the path to the module is a series of nested namespaces that may or may not have been initialized
     */
    uship.namespace = function (namespacePath) {
        var namespaceParts;

        if (arguments.length > 1) {
            namespaceParts = toArray(arguments);
        } else if (isArray(namespacePath)) {
            namespaceParts = namespacePath;
        } else if (typeof namespacePath === 'string') {
            namespaceParts = namespacePath.split('.');
        }

        if (!namespaceParts) throw new Error('Either pass in a single string with dot-separated namespaces, an array of namespace strings, or a separate string param for each namespace');

        if (namespaceParts[0].toLowerCase() === 'uship') {
            namespaceParts = namespaceParts.slice(1);
        }

        return addPartToNamespace(uship, namespaceParts);
    };

    function addPartToNamespace(_x, _x2) {
        var _again = true;

        _function: while (_again) {
            var ns = _x,
                parts = _x2;
            first = undefined;
            _again = false;

            if (parts.length === 0) return ns;
            var first = parts.shift();
            if (!ns[first]) ns[first] = Object.create(nsProto);
            _x = ns[first];
            _x2 = parts;
            _again = true;
            continue _function;
        }
    }

    var nsProto = {
        extend: function extend(source) {
            _extend(this, source);
            return this;
        }
    };

    //Utilities

    function identity(x) {
        return x;
    }

    function noop() {
        return undefined;
    }

    function toType(obj) {
        // IE requires some special-case handling, otherwise returns 'object' for both of the below
        if (obj === null) return 'null';
        if (obj === undefined) return 'undefined';
        // Angus Croll (http://javascriptweblog.wordpress.com/2011/08/08/fixing-the-javascript-typeof-operator)
        return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
    }

    var isArray = Array.isArray || function (obj) {
        return Object.prototype.toString.call(obj) == '[object Array]';
    };

    function isObject(obj) {
        return toType(obj) === 'object';
    }

    function isFunction(obj) {
        return toType(obj) === 'function';
    }

    function isString(obj) {
        return toType(obj) === 'string';
    }

    function isNumber(obj) {
        return toType(obj) === 'number';
    }

    function isTruthy(obj) {
        return !!obj;
    }

    function isBoolean(obj) {
        return toType(obj) === 'boolean';
    }

    function isNullOrUndefined(obj) {
        var type = toType(obj);
        return type === 'null' || type === 'undefined';
    }

    function toArray(args, ix) {
        return Array.prototype.slice.call(args, ix || 0);
    }

    function format(template /*, ...replacements*/) {
        var replacements = toArray(arguments, 1);
        for (var i = 0, j = replacements.length; i < j; i++) {
            template = template.replace(new RegExp('\\{' + i + '\\}', 'g'), replacements[i]);
        }
        return template;
    }

    /**
     * Ensures that the given argument can be called as a function:
     *
     *    var definitelyFn = asCallable(maybeFn);
     *    definitelyFn(); // calling maybeFn would throw if maybeFn is not a function
     *
     * @param {*} maybeFn - value to ensure is callable
     * @param {string|function} instead - function to call if maybeFn is not callable, OR the string 'identity' to return the value of maybeFn
     * @returns {function}
     */
    function asCallable(maybeFn, instead) {
        if (isFunction(maybeFn)) return maybeFn;
        if (isFunction(instead)) return instead;
        if (instead === 'identity') return partial(identity, maybeFn);
        return noop;
    }

    function _extend(_x3, _x4) {
        var _arguments2 = arguments;
        var _again2 = true;

        _function2: while (_again2) /*, ...sources */{
            var target = _x3,
                source = _x4;
            prop = args = undefined;
            _again2 = false;

            if (source) {
                for (var prop in source) {
                    if (source.hasOwnProperty(prop) && typeof source[prop] !== 'undefined') {
                        target[prop] = source[prop];
                    }
                }
            }

            //Recursively apply additional sources
            if (_arguments2.length > 2) {
                var args = toArray(_arguments2, 2);
                args.unshift(target);
                _arguments2 = args;
                _x3 = _arguments2[0];
                _x4 = _arguments2[1];
                _again2 = true;
                continue _function2;
            }

            return target;
        }
    }

    function extendUshipForWebProject(target, source /*, ...sources */) {
        if (source) {
            for (var prop in source) {
                if (source.hasOwnProperty(prop) && source[prop]) {
                    if (!target.hasOwnProperty(prop)) {
                        target[prop] = source[prop];
                    } else {
                        for (var propToExtend in source[prop]) {
                            if (!target[prop].hasOwnProperty(propToExtend)) {
                                target[prop][propToExtend] = source[prop][propToExtend];
                            }
                        }
                    }
                }
            }
        }

        //Recursively apply additional sources
        if (arguments.length > 2) {
            var args = toArray(arguments, 2);
            args.unshift(target);
            return _extend.apply(this, args);
        }

        return target;
    }

    function forEach(obj, fn) {
        if (isArray(obj)) {
            obj.forEach(fn, this);
        } else {
            for (var prop in obj) {
                if (obj.hasOwnProperty(prop)) {
                    fn(prop, obj[prop]);
                }
            }
        }
    }

    function restParams(func, args) {
        if (typeof func !== 'function') return [];
        if (func.length >= args.length) return [];
        var restArgs = toArray(args, func.length);
        if (!restArgs || !restArgs.length) return [];
        return restArgs;
    }

    function flattenToArray() /* arguments */{
        var acc = [],
            args = toArray(arguments);

        args.forEach(function (item) {
            if (isArray(item)) {
                acc = acc.concat(flattenToArray.apply(null, item));
            } else {
                acc.push(item);
            }
        });

        return acc;
    }

    function partial(fn) {
        for (var _len = arguments.length, boundArgs = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
            boundArgs[_key - 1] = arguments[_key];
        }

        return function () {
            for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
                args[_key2] = arguments[_key2];
            }

            return fn.apply(this, boundArgs.concat(args));
        };
    }

    function getUrlParam(key) {
        var result = new RegExp(key + "=([^&]*)", "i").exec(window.location.search);
        return result && result[1] || "";
    }

    function Url(url) {
        var baseUrl = url.split('?')[0].split('#')[0],
            self = this;

        this.queryStringValues = {};
        this.fragmentIdentifier = '';

        if (url.split('#').length > 1) {
            self.fragmentIdentifier = url.split('#')[1];
        }

        if (url.split('?').length > 1) {
            url.split('?')[1].split('&').forEach(function (qsv) {
                var asKvp = qsv.split('=');
                self.queryStringValues[asKvp[0]] = asKvp[1];
            });
        }

        this.addQueryStringParam = function (param, value) {
            this.queryStringValues[param] = value;
        };

        Url.prototype.toString = function urlToString() {
            var fullUrl = baseUrl;

            for (var key in self.queryStringValues) {
                var separator = fullUrl.indexOf('?') > -1 ? '&' : '?';
                fullUrl = fullUrl + separator + key + '=' + self.queryStringValues[key];
            }

            if (self.fragmentIdentifier.length > 1) return fullUrl + '#' + self.fragmentIdentifier;

            return fullUrl;
        };
    }

    // Based on curl.js loader -- simple injection with success/failure events translated to promise resolution
    var activeScripts = {},
        doc = window.document,
        head = doc && (doc['head'] || doc.getElementsByTagName('head')[0]),
        insertBeforeEl = head && head.getElementsByTagName('base')[0] || null,
        readyStates = 'addEventListener' in window ? {} : { 'loaded': 1, 'complete': 1 };

    function injectScript(src) {
        if (activeScripts[src]) return activeScripts[src];

        var el = doc.createElement('script'),
            dfd = $.Deferred();

        function process(ev) {
            ev = ev || window.event;
            if (ev.type == 'load' || readyStates[el.readyState]) {
                el.onload = el.onreadystatechange = el.onerror = '';
                dfd.resolve();
            }
        }

        function fail(ev) {
            dfd.reject(new Error('Syntax or http error: ' + src));
            delete activeScripts[src];
        }

        el.onload = el.onreadystatechange = process;
        el.onerror = fail;
        el.type = 'text/javascript';
        el.charset = 'utf-8';
        el.src = src;

        activeScripts[src] = dfd.promise();

        head.insertBefore(el, insertBeforeEl);

        return dfd.promise();
    }

    var activeCallbackScripts = {};

    function injectCallbackScript(src, callbackName, timeout) {
        if (activeCallbackScripts[src]) return activeCallbackScripts[src];

        timeout || (timeout = 10000);

        var outer = $.Deferred(),
            loadTimeout;

        root[callbackName] = resolveOuter;

        var inject = injectScript(src).fail(failOuter);
        loadTimeout = root.setTimeout(failOuter, timeout);

        return activeCallbackScripts[src] = jQuery.when(inject, outer);

        function resolveOuter(data) {
            root.clearTimeout(loadTimeout);
            outer.resolve(data);
        }

        function failOuter(err) {
            err || (err = new Error('Syntax or http error: ' + src));
            delete activeCallbackScripts[src];
            outer.reject(err);
        }
    }

    function injectTemplate(templateName, templateString, overrideExisting) {
        if (document.getElementById(templateName) && !overrideExisting) return document.getElementById(templateName);

        var el = doc.createElement('script');

        el.type = 'text/html';
        el.id = templateName;
        el.text = templateString;

        doc.body ? doc.body.appendChild(el) : head.insertBefore(el, insertBeforeEl);

        return el;
    }

    // Returns a function, that, as long as it continues to be invoked, will not
    // be triggered. The function will be called after it stops being called for
    // N milliseconds. If `immediate` is passed, trigger the function on the
    // leading edge, instead of the trailing.
    // function debounce(func, wait, immediate) {
    //     var timeout;
    //     return function() {
    //         var context = this, args = arguments;
    //         var later = function() {
    //             timeout = null;
    //             if (!immediate) func.apply(context, args);
    //         };
    //         var callNow = immediate && !timeout;
    //         clearTimeout(timeout);
    //         timeout = setTimeout(later, wait);
    //         if (callNow) func.apply(context, args);
    //     }
    // }

    var debounceOptions = {
        'leading': false,
        'maxWait': 0,
        'trailing': false
    };

    /**
     * Creates a function that will delay the execution of `func` until after
     * `wait` milliseconds have elapsed since the last time it was invoked.
     * Provide an options object to indicate that `func` should be invoked on
     * the leading and/or trailing edge of the `wait` timeout. Subsequent calls
     * to the debounced function will return the result of the last `func` call.
     *
     * Note: If `leading` and `trailing` options are `true` `func` will be called
     * on the trailing edge of the timeout only if the the debounced function is
     * invoked more than once during the `wait` timeout.
     */
    function debounce(func, wait, options) {
        var args,
            maxTimeoutId,
            result,
            stamp,
            thisArg,
            timeoutId,
            trailingCall,
            lastCalled = 0,
            maxWait = false,
            trailing = true;

        if (!isFunction(func)) {
            throw new TypeError();
        }
        wait = Math.max(0, wait) || 0;
        if (options === true) {
            var leading = true;
            trailing = false;
        } else if (isObject(options)) {
            leading = options.leading;
            maxWait = 'maxWait' in options && (Math.max(wait, options.maxWait) || 0);
            trailing = 'trailing' in options ? options.trailing : trailing;
        }

        var delayed = function delayed() {
            var remaining = wait - (Date.now() - stamp);
            if (remaining <= 0) {
                if (maxTimeoutId) {
                    clearTimeout(maxTimeoutId);
                }
                var isCalled = trailingCall;
                maxTimeoutId = timeoutId = trailingCall = undefined;
                if (isCalled) {
                    lastCalled = Date.now();
                    result = func.apply(thisArg, args);
                    if (!timeoutId && !maxTimeoutId) {
                        args = thisArg = null;
                    }
                }
            } else {
                timeoutId = setTimeout(delayed, remaining);
            }
        };

        var maxDelayed = function maxDelayed() {
            if (timeoutId) {
                clearTimeout(timeoutId);
            }
            maxTimeoutId = timeoutId = trailingCall = undefined;
            if (trailing || maxWait !== wait) {
                lastCalled = Date.now();
                result = func.apply(thisArg, args);
                if (!timeoutId && !maxTimeoutId) {
                    args = thisArg = null;
                }
            }
        };

        return function () {
            args = arguments;
            stamp = Date.now();
            thisArg = this;
            trailingCall = trailing && (timeoutId || !leading);

            if (maxWait === false) {
                var leadingCall = leading && !timeoutId;
            } else {
                if (!maxTimeoutId && !leading) {
                    lastCalled = stamp;
                }
                var remaining = maxWait - (stamp - lastCalled),
                    isCalled = remaining <= 0;

                if (isCalled) {
                    if (maxTimeoutId) {
                        maxTimeoutId = clearTimeout(maxTimeoutId);
                    }
                    lastCalled = stamp;
                    result = func.apply(thisArg, args);
                } else if (!maxTimeoutId) {
                    maxTimeoutId = setTimeout(maxDelayed, remaining);
                }
            }
            if (isCalled && timeoutId) {
                timeoutId = clearTimeout(timeoutId);
            } else if (!timeoutId && wait !== maxWait) {
                timeoutId = setTimeout(delayed, wait);
            }
            if (leadingCall) {
                isCalled = true;
                result = func.apply(thisArg, args);
            }
            if (isCalled && !timeoutId && !maxTimeoutId) {
                args = thisArg = null;
            }
            return result;
        };
    }

    /**
     * Creates a function that, when executed, will only call the `func` function
     * at most once per every `wait` milliseconds. Provide an options object to
     * indicate that `func` should be invoked on the leading and/or trailing edge
     * of the `wait` timeout. Subsequent calls to the throttled function will
     * return the result of the last `func` call.
     *
     * Note: If `leading` and `trailing` options are `true` `func` will be called
     * on the trailing edge of the timeout only if the the throttled function is
     * invoked more than once during the `wait` timeout.
     */
    function throttle(func, wait, options) {
        var leading = true,
            trailing = true;

        if (!isFunction(func)) {
            throw new TypeError();
        }
        if (options === false) {
            leading = false;
        } else if (isObject(options)) {
            leading = 'leading' in options ? options.leading : leading;
            trailing = 'trailing' in options ? options.trailing : trailing;
        }
        debounceOptions.leading = leading;
        debounceOptions.maxWait = wait;
        debounceOptions.trailing = trailing;

        return debounce(func, wait, debounceOptions);
    }

    /**
     * Used to intercept events happening in third-party libraries
     *
     * For example, Google Places autocomplete attaches to the enter key in a way we can't override.
     * We can intercept the enter key event and transform it to a different event type,
     * allowing us to work around the default behavior
     */
    function createListenerFacade(el, transformers) {
        if (!el) return;
        var nativeAddListener = el.addEventListener || el.attachEvent;

        function addListenerFacade(type, callback) {
            var tCallback;

            var typeName = type.replace(/on/i, '');
            if (typeName in transformers) {
                tCallback = function (ev) {
                    var transformed = transformers[typeName].call(el, ev);
                    callback.call(el, transformed);
                };
            }

            nativeAddListener.call(el, type, tCallback || callback);
        }

        if (el.addEventListener) el.addEventListener = addListenerFacade;
        if (el.attachEvent) el.attachEvent = addListenerFacade;
    }

    function formatNumber(number, decimals, dec_point, thousands_sep) {
        var n = number,
            c = isNaN(decimals = Math.abs(decimals)) ? 2 : decimals,
            d = dec_point || '.',
            t = thousands_sep || ',',
            str = n < 0 ? '-' : '',
            i = parseInt(n = Math.abs(+n || 0).toFixed(c), 10) + '',
            j = (j = i.length) > 3 ? j % 3 : 0;
        return str + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : '');
    }

    function joinTruthy() {
        var args = toArray(arguments),
            sep = args.pop(),
            toJoin = flattenToArray(args);

        return toJoin.filter(isTruthy).join(sep);
    }

    /**
     * Utility for concatenating HTML element class names into a class list
     *
     * @param {...string|Object} names - strings to concatenate, or KVP where key is the classname and value is boolean
     * @return {string} Space-separated class list
     */
    function classNames() {
        var classes = '';

        for (var _len3 = arguments.length, names = Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
            names[_key3] = arguments[_key3];
        }

        for (var i = 0; i < names.length; i++) {
            var arg = names[i];
            if (!arg) continue;

            if (isString(arg) || isNumber(arg)) {
                classes += ' ' + arg;
            } else if (Array.isArray(arg)) {
                var toAdd = classNames.apply(null, arg);
                if (toAdd) {
                    classes += ' ' + classNames.apply(null, arg);
                }
            } else if (isObject(arg)) {
                for (var key in arg) {
                    if (arg.hasOwnProperty(key) && arg[key]) {
                        classes += ' ' + key;
                    }
                }
            }
        }

        return classes.substr(1);
    }

    /**
     * Returns a copy of an object with specified keys removed
     * 
     * @param  {Object}    obj           The object to copy
     * @param  {...string} keysNotToCopy The keys to excluded from the copied object
     * @return {Object}                  The copied object
     */
    function objectExcept(obj) {
        for (var _len4 = arguments.length, keysNotToCopy = Array(_len4 > 1 ? _len4 - 1 : 0), _key4 = 1; _key4 < _len4; _key4++) {
            keysNotToCopy[_key4 - 1] = arguments[_key4];
        }

        var newObj = {};
        Object.keys(obj).filter(function (k) {
            return keysNotToCopy.indexOf(k) === -1;
        }).forEach(function (k) {
            return newObj[k] = obj[k];
        });
        return newObj;
    }

    /**
     * Helper for Object.prototype.hasOwnProperty
     * 
     * @param  {Object}  obj object to check for key
     * @param  {string}  key key to check for in object
     * @return {Boolean}     Flag indicating presence of key
     */
    function hasProp(obj, key) {
        if (!obj) return false;
        return Object.prototype.hasOwnProperty.call(obj, key);
    }

    function createGuid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
        }

        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    function assert(condition, message) {
        var conditionMet = isFunction(condition) ? condition() : condition;

        if (!conditionMet) {
            if (message instanceof Error) throw message;
            throw new Error(message || 'Assertion failed: ' + condition.toString());
        }
    }

    function failAsync(message) {
        return $.Deferred().reject({ err: message }).promise();
    }

    function genericFactory(Ctor) {
        var args = toArray(arguments, 1),
            inst = Object.create(Ctor.prototype);

        Ctor.apply(inst, args);
        return inst;
    }

    function cachingFactory(Ctor) {
        if (!Ctor.__instances) Ctor.__instances = [];
        var inst = genericFactory.apply(null, arguments);
        Ctor.__instances.push(inst);
        return inst;
    }

    function singletonFactory(Ctor) {
        var args = [Ctor].concat(toArray(arguments, 1));
        if (Ctor.__instances && Ctor.__instances.length) {
            return Ctor.__instances[0];
        }
        return cachingFactory.apply(null, args);
    }

    var $window = $(window);

    function viewPortDimensions() {
        var container = window;
        var a = 'inner';
        if (!window.innerWidth) {
            a = 'client';
            container = document.documentElement || document.body;
        }
        var w = a + 'Width',
            h = a + 'Height';
        //return a func because the return values may change everytime the user tries to get dimensions.
        return function () {
            return {
                width: container[w],
                height: container[h],
                innerWidth: $window.width(),
                innerHeight: $window.height()
            };
        };
    }

    var utils = uship.namespace('utils').extend({
        identity: identity,
        noop: noop,
        toType: toType,
        isObject: isObject,
        isFunction: isFunction,
        isString: isString,
        isArray: isArray,
        isNumber: isNumber,
        isBoolean: isBoolean,
        isTruthy: isTruthy,
        isNullOrUndefined: isNullOrUndefined,
        toArray: toArray,
        format: format,
        extend: _extend,
        forEach: forEach,
        flattenToArray: flattenToArray,
        partial: partial,
        getUrlParam: getUrlParam,
        asCallable: asCallable,
        Url: Url,
        injectScript: injectScript,
        injectCallbackScript: injectCallbackScript,
        injectTemplate: injectTemplate,
        restParams: restParams,
        createListenerFacade: createListenerFacade,
        formatNumber: formatNumber,
        debounce: debounce,
        throttle: throttle,
        joinTruthy: joinTruthy,
        objectExcept: objectExcept,
        hasProp: hasProp,
        createGuid: createGuid,
        assert: assert,
        failAsync: failAsync,
        genericFactory: genericFactory,
        cachingFactory: cachingFactory,
        singletonFactory: singletonFactory,
        classNames: classNames,
        viewPortDimensions: viewPortDimensions
    });

    //Polyfill helpers

    var placeholder = function placeholder(context, isSelf) {
        if (!$ || !$.fn.placeholder) return;
        context = context || $(document);
        $(function () {
            isSelf ? context.placeholder() : context.find('input, textarea').placeholder();
        });
    };

    // IE8 Does not fire the change event before the submit event,
    // therefore the KO VM is stale and must be updated manually.

    var refreshModelFromDom = function refreshModelFromDom(scope, model, callback) {
        if (!$) return;

        scope = scope || 'body';

        var inputs = $(scope).find(':input');

        $.each(inputs, function (index, input) {
            var name = input.name;

            if (!model[name] || typeof model[name] !== 'function') return;

            if (input.value !== model[name]()) {
                model[name](input.value);
            }
        }); // $.each

        if (callback) callback();
    };

    uship.namespace('polyfills').extend({
        placeholder: placeholder,
        refreshModelFromDom: refreshModelFromDom
    });

    //Common helpers
    if (typeof root.console === 'undefined') {
        root.console = {
            log: noop
        };
    }

    var noopAlert = {
        log: noop,
        success: noop,
        error: noop,
        debug: noop
    };

    uship.namespace('helpers').extend({
        alert: noopAlert,
        noopAlert: noopAlert
    });

    //log errors that happened before documentReady
    $ && $(function () {
        root.logging && root.logging.logQueue;
    });

    if (previousuShip && (window.location.pathname.indexOf('listing2.aspx') > 0 || window.location.pathname.indexOf('interview_listing.aspx') > 0)) {
        extendUshipForWebProject(uship, previousuShip);
    } else if (previousuShip) {
        _extend(uship, previousuShip);
        previousuShip.utils && _extend(uship.utils, utils);
    }
})(window, jQuery);