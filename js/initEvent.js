'use strict';

;(function (root, $, Modernizr, uship) {
    'use strict';

    var trackingChannel = uship.messageBus('TRACKING_CHANNEL'),
        uiChannel = uship.messageBus('UI_CHANNEL');

    var PERCENT_VISIBLE = 0.75,
        SCROLL_THROTTLE_INTERVAL = 200,
        RESIZE_DEBOUNCE_INTERVAL = 100,
        HERO_VIDEO_WIDTH = 1280.0,
        HERO_VIDEO_HEIGHT = 720.0,
        HERO_VIDEO_ASPECT_RATIO = HERO_VIDEO_HEIGHT / HERO_VIDEO_WIDTH;

    var canPlayHeroVideo = Modernizr.video,
        homeModel = uship.namespace('bootstrap.viewData'),
        $window,
        panels,
        viewportHeight,
        viewportWidth,
        panelOffsets,
        lastScrollPosition,
        currentScrollPosition,
        header,
        heroPanel,
        heroPanelOffset,
        heroVideo,
        heroImage,
        heroCaret,
        tabletAndUpBreakpoint;

    $(function init() {

        $window = $(root);
        panels = $('.js-featurePanel');
        viewportHeight = $window.height();
        viewportWidth = $window.width();
        header = $('#hd');
        heroPanel = $('#hero-video-panel');
        heroVideo = $('#hero-video');
        heroImage = $('#hero-image');
        heroCaret = $('#hero-caret');

        tabletAndUpBreakpoint = root.matchMedia ? root.matchMedia('(min-width: 768px)') : tabletAndUpBreakpoint = { matches: true };

        heroCaret.on('click', dismissVideo);

        $window.on({
            'scroll.header': uship.utils.throttle(setHeaderTransparency, SCROLL_THROTTLE_INTERVAL),
            'scroll.panels': uship.utils.throttle(setPanelVisibility, SCROLL_THROTTLE_INTERVAL),
            'scroll.herocaret': uship.utils.throttle(setHeroCaretVisibility, SCROLL_THROTTLE_INTERVAL),
            'resize': uship.utils.throttle(calculateDimensions, RESIZE_DEBOUNCE_INTERVAL),
            'recalculatedDimensions.panels': setPanelVisibility,
      //      'recalculatedDimensions.video': handleVideoLoad
        });

        calculateDimensions();
        initMapBox();
        publishLandedOnHomePage();
    });

    function publishLandedOnHomePage() {
        trackingChannel.publish('landedOnHomePage');
    }

    function dismissVideo() {
        var headerHeight = header.height(),
            offset = viewportHeight - headerHeight;

        $('html, body').animate({
            scrollTop: offset
        }, 800);

        trackingChannel.publish('dismissedVideo');
    }

    function calculatePanelIllustrationOffset(panel) {
        var illustration = panel.find('.js-illustration');
        illustration = illustration.length ? illustration.first() : panel;

        var fromTop = illustration.offset().top;
        return {
            top: fromTop,
            bottom: fromTop + illustration.height(),
            el: panel
        };
    }

    function calculateDimensions() {
        viewportHeight = $window.height();
        viewportWidth = $window.width();
        //setVideoPanelDimensions();
        //heroPanelOffset = calculatePanelIllustrationOffset(heroPanel);
        panelOffsets = panels.map(function () {
            return calculatePanelIllustrationOffset($(this));
        }).get();
        $window.trigger('recalculatedDimensions');
    }

    function initMapBox() {
        if ($('#map').length > 0) {
            setupMapBox();
        }
    }

    function setupMapBox() {
        var zoomLevel = homeModel.mapZoomLevel;
        if (tabletAndUpBreakpoint.matches) zoomLevel = homeModel.mapDesktopZoomLevel;
        L.mapbox.accessToken = 'pk.eyJ1IjoidXNoaXAiLCJhIjoiSzNwTkpaMCJ9.ibcmzj7-eiivBarcYBqwHw';
        var map = L.mapbox.map('map', 'uship.lgig7jdj', {
            minZoom: 3,
            maxZoom: 11,
            zoomControl: false,
            keyboard: false,
            attributionControl: false
        }).setView([homeModel.mapLatitude, homeModel.mapLongitude], zoomLevel);
        map.scrollWheelZoom.disable();
        if (L.Browser.mobile || L.Browser.touch || L.Browser.msTouch) {
            map.dragging.disable();
            map.tap && map.tap.disable();
        }
        new L.Control.Zoom({ position: 'bottomleft' }).addTo(map);
    }
/*
    function setVideoPanelDimensions() {
        if (!tabletAndUpBreakpoint.matches) {
            if (!setVideoPanelDimensions.videoHidden) {
                setVideoPanelDimensions.videoHidden = true;
                heroPanel.css('height', '');
            }
        } else {
            if (canPlayHeroVideo) {
                var viewportAspectRatio = viewportHeight / viewportWidth;
                if (viewportAspectRatio > HERO_VIDEO_ASPECT_RATIO) {
                    var calculatedWidth = viewportHeight * HERO_VIDEO_WIDTH / HERO_VIDEO_HEIGHT;
                    heroVideo[0].height = viewportHeight;
                    heroVideo[0].width = calculatedWidth;
                } else {
                    var calculatedHeight = viewportWidth * HERO_VIDEO_HEIGHT / HERO_VIDEO_WIDTH;
                    heroVideo[0].width = viewportWidth;
                    heroVideo[0].height = calculatedHeight;
                }
            }

            setVideoPanelDimensions.videoHidden = false;
            heroPanel.css('height', viewportHeight + 'px');
        }
    }
*/
    function setHeaderTransparency() {
        hasScrolled() ? makeHeaderOpaque() : makeHeaderTransparent();
    }

    function makeHeaderTransparent() {
        header.addClass('is-transparent');
    }

    function makeHeaderOpaque() {
        header.removeClass('is-transparent');
    }

    function setPanelVisibility() {
        lastScrollPosition = currentScrollPosition;
        currentScrollPosition = $window.scrollTop();

        var visiblePanels = panelOffsets.filter(function (x) {
            var panelHeight = x.bottom - x.top,
                visibleHeight = calculateVisibleHeight(x.top, x.bottom);

            return visibleHeight / panelHeight > PERCENT_VISIBLE;
        });

        visiblePanels.forEach(function (x) {
            if (x.el.hasClass('is-visible')) return;

            x.el.addClass('is-visible');

            uiChannel.publish('panelBecameVisible');
            trackingChannel.publish('panelBecameVisible.track', {
                panel: x.el.data('featurePanel')
            });
        });
    }

    function setHeroCaretVisibility() {
        hasScrolled() ? dismissHeroCaret() : summonHeroCaret();
    }

    function dismissHeroCaret() {
        if (setHeroCaretVisibility.dismissed) return;

        heroCaret.addClass('dismissed');
        setHeroCaretVisibility.dismissed = true;
    }

    function summonHeroCaret() {
        if (!setHeroCaretVisibility.dismissed) return;

        heroCaret.removeClass('dismissed');
        setHeroCaretVisibility.dismissed = false;
    }
/*
    function handleVideoLoad() {
        if (tabletAndUpBreakpoint.matches && canPlayHeroVideo) {
            if (!handleVideoLoad.isLoaded) {
                handleVideoLoad.isLoaded = true;

                trackingChannel.publish('videoPanelDisplayed.track');

                heroVideo.on('canplay', function () {
                    heroVideo[0].play();
                    trackingChannel.publishOnce('videoStarted.track');
                });

                if (heroVideo[0].readyState >= heroVideo[0].HAVE_FUTURE_DATA) {
                    heroVideo.trigger('canplay');
                } else {
                    heroVideo[0].preload = "auto";
                    heroVideo[0].load();
                }
            }
            heroVideo.show();
            heroImage.hide();
        } else {
            heroImage.show();
            heroVideo.hide();
            trackingChannel.publishOnce('staticHeroImageDisplayed.track');
        }
    }
*/
    function hasScrolled() {
        currentScrollPosition = $window.scrollTop();

        return currentScrollPosition > 10;
    }

    function calculateVisibleHeight(top, bottom) {
        return Math.min(bottom, currentScrollPosition + viewportHeight) - Math.max(top, currentScrollPosition);
    }
})(window, jQuery, Modernizr, uship);

/**
 * Attach analytics events
 */

;(function ($, uship, tracking, data) {
    'use strict';

    var trackingChannel = uship.messageBus('TRACKING_CHANNEL'),
        userData = 1;//data.userData;

    tracking.proxy('event', tracking.proxies.consoleLogEvent);
    // tracking.proxy('linkClick', tracking.proxies.consoleLogLinkClick);

    var eventFullNames = {
        panelBecameVisible: 'Scrolled to feature panel',
        videoPanelDisplayed: 'Activated hero video',
        staticHeroImageDisplayed: 'Activated hero image',
        videoStarted: 'Hero video started playing',
        activatedTestimonial: 'Viewed testimonial text',
        dismissedVideo: 'Clicked caret to dismiss hero panel',
        landedOnHomePage: 'Landed on the new home page',
        zoomInOnMap: 'Clicked zoom in on interactive map',
        zoomOutOnMap: 'Clicked zoom out on interactive map'
    };

    trackingChannel.subscribe('*.track', function (data, message) {
        var eventToTrack = eventFullNames[message.eventName];
        if (!eventToTrack) return;
        tracking.event(eventToTrack, message.data);
    });

    if (userData.userId > 0) {
        tracking.identify(userData.userId);
    }

    tracking.registerOnce({
        referrer: userData.referrerId
    });

    tracking.register({
        isAuthenticated: userData.isAuthenticated,
        userType: userData.userType || 'Unknown',
        uShipSite: userData.Site
    });

    function zoomInOnMap() {
        trackingChannel.publish('zoomInOnMap');
    }

    function zoomOutOnMap() {
        trackingChannel.publish('zoomOutOnMap');
    }

    $(function () {
        tracking.linkClick('#hero-cta-link', 'Clicked hero panel call to action');
        tracking.linkClick('#header-signIn-link', 'Clicked header Sign In');
        tracking.linkClick('#header-join-link', 'Clicked header Join');
        tracking.linkClick('#header-findShipments-link', 'Clicked header Find Shipments');
        tracking.linkClick('#header-support-link', 'Clicked header Support');
        tracking.linkClick('#footer-cta-link', 'Clicked footer call to action');
        tracking.linkClick('#logo', 'Clicked header uShip logo');
        tracking.linkClick('#header-ship-button', 'Clicked header Ship button');
        $('.leaflet-control-zoom-in').on('click', zoomInOnMap);
        $('.leaflet-control-zoom-out').on('click', zoomOutOnMap);
    });
})(jQuery, uship, uship.namespace('track'), uship.namespace('bootstrap.viewData'));

/**
 * Attach testimonials event handlers
 */

;(function ($, Modernizr, uship) {
    'use strict';

    var trackingChannel = uship.messageBus('TRACKING_CHANNEL'),
        uiChannel = uship.messageBus('UI_CHANNEL');

    var supportsTransitions = Modernizr.csstransitions,
        transitionEnd = getTransitionEndEventName(),
        onTransition = supportsTransitions ? attachTransitionListener : uship.utils.noop;

    var css = {
        copySelector: '.js-testimonial-copy',
        buttonSelector: '.js-testimonial-button',
        activeClass: 'is-active',
        transitioningClass: 'is-transitioning'
    };

    uiChannel.subscribe('panelBecameVisible', handlePanelBecameVisible);

    $(function () {

        var testimonialsPanel = $('#testimonials'),
            testimonialCopy = testimonialsPanel.find(css.copySelector),
            testimonialButtons = testimonialsPanel.find(css.buttonSelector);

        testimonialCopy.on('click', deactivateTestimonial);
        testimonialButtons.on('change', handleTestimonialChange);
        testimonialsPanel.one('panelBecameVisible', handlePanelBecameVisible);
        testimonialCopy.each(onTransition);
    });

    function handlePanelBecameVisible() {
        var secondTestimonial = $('#testimonials').find(css.copySelector).slice(1, 2),
            radioEl = secondTestimonial.parent().find(css.buttonSelector);

        radioEl.prop('checked', true);
        setActiveTestimonial(secondTestimonial);
    }

    function attachTransitionListener() {
        this.addEventListener(transitionEnd, function (event) {
            if (event.propertyName !== 'opacity') return;
            $(event.target).removeClass(css.transitioningClass);
        });
    }

    function handleTestimonialChange(event) {
        var newlyActive = $(event.target).parent(),
            copyEl = newlyActive.find(css.copySelector);

        setActiveTestimonial(copyEl);

        trackingChannel.publish('activatedTestimonial', {
            testimonial: newlyActive.data('testimonialName')
        });
    }

    function setActiveTestimonial(newlyActive) {
        if (setActiveTestimonial.previouslyActive) {
            supportsTransitions && setActiveTestimonial.previouslyActive.addClass(css.transitioningClass);
            setActiveTestimonial.previouslyActive.removeClass(css.activeClass);
        }

        if (newlyActive) {
            supportsTransitions && newlyActive.addClass(css.transitioningClass);
            newlyActive.addClass(css.activeClass);
        }

        setActiveTestimonial.previouslyActive = newlyActive;
    }

    function deactivateTestimonial(event) {
        var previous = setActiveTestimonial.previouslyActive;

        if (!previous || previous[0] !== event.target) return;

        var copyEl = $(event.target),
            radioEl = copyEl.parent().find(css.buttonSelector);

        radioEl.prop('checked', false);
        setActiveTestimonial(null);

        event.preventDefault();
        event.stopPropagation();
    }

    function getTransitionEndEventName() {
        var transEndEventNames = {
            'WebkitTransition': 'webkitTransitionEnd', // Saf 6, Android Browser
            'MozTransition': 'transitionend', // only for FF < 15
            'transition': 'transitionend' // IE10, Opera, Chrome, FF 15+, Saf 7+
        };
        return transEndEventNames[Modernizr.prefixed('transition')];
    }
})(jQuery, Modernizr, uship);